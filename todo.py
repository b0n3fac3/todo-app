#!/usr/bin/python3
import os
import sys

todo_list = []

# save list to database file
def save_to_database():
    with open("Database", "a") as database:
        database.write(str(todo_list))

# delete database or reset it or create multiple
def delete_database():
    os.remove("./Database")

# create database file
def create_database():
    os.system('touch ./Database')

# print todo_list
def read_todo():
    with open("Database", "r") as database:
        for data in database:
            print(data)

# append item to list
def add_to_list():
    todo = input("ToDo: ")
    todo_list.append(todo)

def main():
#    add_to_list()
#    save_to_database()
#    read_todo()
    if len(sys.argv) !=2:
        raise ValueError('help for options')
    elif sys.argv[1] == 'add':
        add_to_list()
        save_to_database()
        read_todo()
    elif sys.argv[1] == 'clear':
        delete_database()
        create_database()
    elif sys.argv[1] == 'read':
        read_todo()
    elif sys.argv[1] == 'help':
        print('there is no help' + '\n')
     
if __name__ == '__main__':
    main()
